import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook'
import { GooglePlus } from '@ionic-native/google-plus';
import { HomePage } from '../home/home';
import { NativeStorage } from '@ionic-native/native-storage';
import { Platform } from 'ionic-angular';
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  userProfile: any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, private facebook: Facebook, private google: GooglePlus, private storage:NativeStorage, private plt: Platform) {
    this.plt.ready().then((readySource) =>{
      console.log("Platform ready from", readySource);
      this.storage.getItem('userInfo')
        .then(
          data => {
            this.navCtrl.setRoot(HomePage)
          },
          error => {return}
      );
    })

  }

  facebookLogin() {
    this.facebook.login(['public_profile', 'email']).then((response) => {
      console.log("Initial Facebook response", response);

      this.facebook.api(response.authResponse.userID+"/?fields=id,email,name,picture.type(large)",["public_profile"])
          .then((resp) => {
            console.log("Actual Facebook response", resp);
            this.storage.setItem(
              'userInfo', {acct_type: 'facebook', 
              user_id: resp.id,
              name: resp.name,
              email:resp.email, 
              picture: resp.picture.data.url
            }).then(
              () => {
                console.log('Saved Facebook data to localstorage!')
                this.navCtrl.setRoot(HomePage)
              },
              error => console.error('Error storing item', error)
            );

          }, (err) => {
            console.error(err)
          })
    }).catch((error) => { console.log(error) });
  }

  googleLogin(){
    this.google.login({
      'scopes': 'https://www.googleapis.com/auth/plus.me', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      'offline': true,
    }).then((response) =>{
      console.log("Google response", response);
      this.storage.setItem(
              'userInfo', {acct_type: 'facebook', 
              user_id: response.userId,
              name: response.displayName,
              email:response.email, 
              picture: response.picture.data.url
            });
    }).then(()=>{
      console.log("Saved Google data to localstorage");
       this.navCtrl.setRoot(HomePage);
    }).catch((error) => {console.log(error) });
  }
}
