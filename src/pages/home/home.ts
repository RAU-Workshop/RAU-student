import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook'
import { NativeStorage } from '@ionic-native/native-storage';
import { Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
import { IBeacon } from '@ionic-native/ibeacon';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   
  userProfile   : any;
  studentId : String;
  courses : any;
  activeCourse : any;
  present: boolean;
  studentToken : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private facebook: Facebook, private storage:NativeStorage, private plt: Platform, private web: Http, private alertController:AlertController, private ibeacon: IBeacon, private localNotifications:LocalNotifications) {
    this.present = false;
     this.plt.ready().then((readySource) =>{
      console.log("Platform ready, loading storageData");
      this.storage.getItem('userInfo')
        .then(
          data => {
            this.userProfile = data;
            this.storage.getItem('studentId').then(data=>{this.studentId = data });
          },
          error => {return}
      );
    })

  }

  backendLogin(){
    console.log("logging in to backend");
      this.web.post('http://rau.bzz.click/api/users',{
          'name':this.userProfile.name,
          'email': this.userProfile.email,
          'password':'abcdefgh',
          'imageURL':this.userProfile.picture
        }).map(res=>res.json()).subscribe(
           (data) => {
            console.log("token: ", data);
            this.storage.setItem('userToken', data.id);
            this.studentToken = data.id;
             this.web.get('http://rau.bzz.click/api/courses').map(res => res.json()).subscribe(
        (data) => {
         
          this.courses = data;
          console.log("(s)Cursurile",this.courses);
          let today = new Date();
          // let course : any;
          this.courses.forEach(course => {
         
            let cDate = new Date(course.date);
            
            if ( cDate.getMonth() == today.getMonth() && cDate.getDate() == today.getDate() && cDate.getFullYear() == today.getFullYear() ) {
              this.activeCourse = course;
              console.log("(s)Cursul activ:", this.activeCourse);
              this.rangeBeacons();
            }
          });
         }
      )
      });

      
  }

  fakeBackendLogin(){
    //alert ("Dis fake! I'm de captain now!");
    let alert = this.alertController.create({
      title: 'Dis fake!',
      subTitle: 'Y\'m de captain now!',
      buttons: ['OK']
    });
    alert.present();
  }
  rangeBeacons(){
      // Request permission to use location on iOS
      this.ibeacon.requestAlwaysAuthorization();
      // create a new delegate and register it with the native layer
      let delegate = this.ibeacon.Delegate();
      let beaconRegion = this.ibeacon.BeaconRegion('iotBeacon','713D0000-503E-4C75-BA94-3148F18D941E',0,0,true);
       
      // Subscribe to some of the delegate's event handlers
      delegate.didRangeBeaconsInRegion()
        .subscribe(
          data => {
            console.log('didRangeBeaconsInRegion: ', data);
           //console.log("checking for token", this.studentToken);
            //Get accurate region positioning, for example, check if data.beacons has at least 4 beacons
            //and at least 4 beacons have Far region
            //if all 4 have near region then confirm presence to server
            //for our workshop we just check for one
            data.beacons.forEach(beacon => {
              console.log(beacon.proximity);
                if ((beacon.proximity == "ProximityFar" || beacon.proximity == "ProximityNear") && this.present == false) {
                  console.log("Announcing to server");
                  let url = 'http://rau.bzz.click/api/users/'+this.studentToken+'/detected';
                  console.log(url);
                  this.web.get(url).map(res => res.json()).subscribe(
                    (data) => {
                      console.log(data);
                      this.present = true;
                    } );
                }
            });
          },
          error => console.error()
        );
      delegate.didStartMonitoringForRegion()
        .subscribe(
          data => console.log('didStartMonitoringForRegion: ', data),
          error => console.error()
        );
      delegate.didEnterRegion()
        .subscribe(
          data => {
            console.log('didEnterRegion: ', data);
          }
        );
        delegate.didDetermineStateForRegion()
        .subscribe(
          data => {
             if(data.state === "CLRegionStateInside"){
               console.log(data);
               this.ibeacon.startRangingBeaconsInRegion(beaconRegion);
               this.localNotifications.schedule({
                  id: 1,
                  text: 'Welcome to school!',
                  sound:  'file://beep.caf',
                  data: null
               });
               
             } else if(data.state === "CLRegionStateOutside"){
               this.ibeacon.stopRangingBeaconsInRegion(beaconRegion);
               console.log("Announcing to server");
                  let url = 'http://rau.bzz.click/api/users/'+this.studentToken+'/left';
                  console.log(url);
                  this.web.get(url).map(res => res.json()).subscribe(
                    (data) => {
                      console.log(data);
                      this.present = false;
                    } );
              this.localNotifications.schedule({
                  id: 2,
                  text: 'Goodbye! N-ai restanta, n-ai prestanta! :)',
                  sound:  'file://beep.caf',
                  data: null
               });
            }
          }
        );


     

      this.ibeacon.startMonitoringForRegion(beaconRegion)
        .then(
          () => {
            console.log('Native layer recieved the request to monitoring')
            this.ibeacon.startMonitoringForRegion(beaconRegion);
            
          },
          error => console.error('Native layer failed to begin monitoring: ', error)
        );
        }

}
